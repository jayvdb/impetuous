"""empty message

Revision ID: ea11786324dd
Revises: f4aaf1bdf8c0
Create Date: 2018-03-17 00:37:08.218060

"""
from alembic import op
import sqlalchemy as sa


import impetuous.data

# revision identifiers, used by Alembic.
revision = 'ea11786324dd'
down_revision = 'f4aaf1bdf8c0'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_index('idx_entry_end', 'entry', ['end'], unique=False)
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_index('idx_entry_end', table_name='entry')
    # ### end Alembic commands ###
