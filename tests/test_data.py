import itertools
import logging
from datetime import timedelta as delta
from datetime import datetime

import pytest
from sqlalchemy.exc import IntegrityError, StatementError
from sqlalchemy.sql import expression

from impetuous.data import entry_t
from tests import now

logger = logging.getLogger(__name__)


def minutes(n):
    return now + delta(minutes=n)


def seconds(n):
    return now + delta(seconds=n)


def test_rev_inc(gen):
    entry, =\
    gen.ins_entry(start=now)
    rev1, = gen.sel_entry(entry, 'rev')
    gen.upd_entry(entry, text='potato')
    rev2, = gen.sel_entry(entry, 'rev')
    assert rev1 != rev2


def _range_string(ranges):
    return " & ".join(
        "[%s-%s)" % (r["start"].strftime("%M:%S"), r["end"].strftime("%M:%S"))
        for r in ranges
    )


@pytest.fixture(
    params=[
        # [0-1) [0-2)
        ({"start": seconds(0), "end": seconds(1)}, {"start": seconds(0), "end": seconds(2)}),
        ({"start": seconds(0), "end": seconds(2)}, {"start": seconds(0), "end": seconds(1)}),
        # [1-2) [0-2)
        ({"start": seconds(1), "end": seconds(2)}, {"start": seconds(0), "end": seconds(2)}),
        ({"start": seconds(0), "end": seconds(2)}, {"start": seconds(1), "end": seconds(2)}),
        # [0-3) [1-2)
        ({"start": seconds(0), "end": seconds(3)}, {"start": seconds(1), "end": seconds(2)}),
        ({"start": seconds(1), "end": seconds(2)}, {"start": seconds(0), "end": seconds(3)}),
    ],
    ids=_range_string,
)
def overlapping(request):
    return request.param


def test_insert_overlapping_entries(gen, overlapping):
    first, second = overlapping
    gen.ins_entry(**first)
    with pytest.raises(IntegrityError):
        gen.ins_entry(**second)


def test_update_overlapping_entries(gen, overlapping):
    first, second = overlapping
    uuid, = gen.ins_entry(start=now + delta(minutes=42), end=now + delta(minutes=43))
    gen.ins_entry(**second)
    with pytest.raises(IntegrityError):
        gen.upd_entry(uuid, **first)


@pytest.fixture(
    params=itertools.permutations([
        {"start": seconds(0), "end": seconds(1)},
        {"start": seconds(1), "end": seconds(2)},
        {"start": seconds(2), "end": seconds(3)},
    ]),
    ids=_range_string,
)
def not_overlapping(request):
    return request.param


def test_entries_not_overlapping(gen, not_overlapping):
    first, second, third = not_overlapping
    gen.ins_entry(**first)
    gen.ins_entry(**second)
    gen.ins_entry(**third)


def test_change_entry_time(gen):
    last, =\
    gen.ins_entry(start=now, end=minutes(3))
    gen.upd_entry(last, end=minutes(4))


def test_precision(gen):
    with pytest.raises(ValueError):
        gen.ins_entry(start=now, end=now + delta(microseconds=123))


def test_insert_benchmark(gen, benchmark):
    counter = itertools.count()
    def insert():
        i = next(counter)
        gen.ins_entry(start=now + delta(minutes=i),
                      end=now + delta(minutes=i + 1))
    benchmark(insert)


def test_select_benchmark(gen, benchmark):
    for i in range(1000):
        gen.ins_entry(start=now + delta(minutes=i),
                      end=now + delta(minutes=i + 1))
    q = expression.select([entry_t.c.id]).order_by(entry_t.c.end.desc())
    def select():
        return gen.conn.execute(q)
    r = benchmark(select)
    assert len(list(r)) == 1000
