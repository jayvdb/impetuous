"""
Tests for exectuing requests and request stuff in interaction.py and a bit of
serialization stuff too ...
"""
import logging
import textwrap
from datetime import datetime, timedelta
from pprint import pprint
from uuid import UUID

import attr
import pytest
from sqlalchemy.exc import IntegrityError

from impetuous.interaction import (Comparison, Conjunction, DeleteRequest,
                                   Field, FindRequest, Gather, InsertRequest,
                                   Interaction, Param, UpdateRequest,
                                   impetuous_access_control)
from impetuous.serialization import deserialize_request
from tests import now

logger = logging.getLogger(__name__)


@pytest.fixture
def funny_data(engine):
    from impetuous.data import entry_t, metadata
    from sqlalchemy.sql import expression

    with engine.connect() as c:
        metadata.create_all(c)

    def insert(table, data):
        ins = expression.insert(table).values(**data)
        with engine.connect() as conn:
            resp = conn.execute(ins)
        return resp.inserted_primary_key

    banana_0 = insert(entry_t, {
        'text': "found bananas",
        'comment': "they were in a banana orchard",
        'start': now,
        'end': now + timedelta(minutes=5)})[0]

    banana_1 = insert(entry_t, {
        'text': "ate bananas",
        'start': now + timedelta(minutes=5),
        'end': now + timedelta(minutes=10)})[0]

    naptime = insert(entry_t, {
        'text': "naptime",
        'start': now + timedelta(minutes=10)})[0]

    return {
        "banana_0": banana_0,
        "banana_1": banana_1,
        "naptime": naptime,
    }


def test_deserialize_FindRequest():
    request = deserialize_request(textwrap.dedent(
        """
        !im/find
        gather: !im/gather
          fields:
            - !im/field
              at: comment
              name: text
            - !im/field
              at: entry
              name: text
              alias: entry_text
          match: !im/cmp
          - !im/field
            at: entry
            name: text
          - like
          - !im/param search
          relating:
          - comment entries
        using:
          search: '%bananas%'
        """))
    expected = FindRequest(
        gather=Gather(
            match=Comparison(Field('entry', 'text'), 'like', Param('search')),
            relating=['comment entries'],
            fields=[
                Field('comment', 'text'),
                Field('entry', 'text', 'entry_text'),
            ],
            #fields={
            #    "text": Field('comment', 'text'),
            #    "entry": {
            #        "text": Field('entry', 'text'),
            #    },
            #},
        ),
        using={'search': '%bananas%'},
    )
    assert request == expected


def test_deserialize_InsertRequest():
    request = deserialize_request(textwrap.dedent(
        f"""
        !im/insert
        resource: entry
        values:
          text: planted new banana orchard
          start: {now + timedelta(minutes=20)}
        """))
    expected = InsertRequest(
        resource='entry',
        values={
            "text": "planted new banana orchard",
            "start": now + timedelta(minutes=20),
        },
    )
    assert request == expected


def test_deserialize_UpdateRequest():
    request = deserialize_request(textwrap.dedent(
        f"""
        !im/update
        resource: entry
        id: 00000000-0000-0000-0000-000000000000
        rev: 00000000-0000-0000-0000-000000000000
        values:
          end: {now + timedelta(minutes=30)}
        """))
    expected = UpdateRequest(
        resource='entry',
        id=UUID('00000000-0000-0000-0000-000000000000'),
        rev=UUID('00000000-0000-0000-0000-000000000000'),
        values={"end": now + timedelta(minutes=30)},
    )
    assert request == expected


@pytest.mark.skip("Doesn't work because comments are gone")
def test_find_interaction(conn, funny_data, agent):
    find = FindRequest(
        gather=Gather(
            match=Comparison(Field('entry', 'text'), 'like', Param('search')),
            relating=['comment entries'],
            fields=[
                Field('comment', 'text'),
                Field('entry', 'text', 'entry_text'),
            ],
        ),
        using={'search': '%bananas%'},
    )
    i = Interaction(conn, agent, impetuous_access_control)
    with conn.begin():
        res = i.find(gather=find.gather, using=find.using)
        assert res == [
            {'text': 'they were in a banana orchard', 'entry_text': 'found bananas'},
            {'text': 'this is a banana comment', 'entry_text': 'ate bananas'},
            {'text': 'so is this', 'entry_text': 'ate bananas'},
        ]


@pytest.mark.skip("Doesn't work because comments are gone")
def test_insert_interaction(conn, funny_data, agent):
    naptime_comment = {
        "entry": funny_data['naptime'],
        "position": 1,
        "text": "this is a naptime comment",
    }
    insert = InsertRequest(
        resource='comment',
        values=naptime_comment,
    )
    i = Interaction(conn, agent, impetuous_access_control)
    with conn.begin():
        inserted = i.insert(resource=insert.resource, values=insert.values)
        assert inserted
        res = i.find(
            gather=Gather(
                match=Comparison(Field('comment', 'id'), 'eq', Param('id')),
                fields=[
                    Field('comment', 'entry'),
                    Field('comment', 'position'),
                    Field('comment', 'text')]),
            using=inserted,
        )
        assert res == [naptime_comment]


def test_update_interaction(conn, funny_data, agent):
    update = UpdateRequest(
        resource='entry',
        id=funny_data['naptime'],
        values={"text": "ate exactly two bananas"},
    )
    i = Interaction(conn, agent, impetuous_access_control)
    with conn.begin():
        i.update(resource=update.resource, id=update.id, values=update.values)
        test = i.find(
            gather=Gather(
                match=Comparison(Field('entry', 'id'), 'eq', Param('id')),
                fields=[Field('entry', 'text')],
            ),
            using={"id": funny_data['naptime']},
            map_items=dict,
        )
        assert tuple(test) == ({"text": "ate exactly two bananas"},)
