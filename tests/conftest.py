"""
https://docs.pytest.org/en/latest/fixture.html#conftest-py-sharing-fixture-functions
"""
import pytest

from tests.fixtures import *


@pytest.fixture(scope='session', autouse=True)
def install_l10n():
    import gettext
    tr = gettext.translation('spam', 'locale', fallback=True)
    tr.install('gettext')
