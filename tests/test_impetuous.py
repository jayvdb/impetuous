import configparser
import datetime
from datetime import timedelta
import itertools
import unittest
import unittest.mock

import pytest
import pytz

from impetuous.config import CodedValue
from impetuous.ext import LudicrousConditions
from impetuous.ext.jira import Jira
from impetuous.im import Impetuous
from impetuous.sheet import Entry


class JIRAPost(unittest.TestCase):
    def setUp(self):
        self.now = datetime.datetime(2010, 5, 4, 1, 0, 0, tzinfo=pytz.utc)
        self.im = Impetuous()
        self.jira = Jira(
            pattern=r"(.+)", server="gopher://localhost", basic_auth=":hunter2"
        )
        super().setUp()

    def test_comment(self):
        entry = Entry(
            text="test",
            start=self.now.replace(minute=0, second=0),
            end=self.now.replace(minute=1, second=30),
            comment="Hello world",
        )
        _, submission = next(self.jira.discover(self.im, entry))
        self.assertEqual(submission.data["comment"], "Hello world")

    def test_comment(self):
        entry = Entry(
            text="test",
            start=self.now.replace(minute=0, second=0),
            end=self.now.replace(minute=1, second=30),
            comment="- foobar\n- spam\n- eggs",
        )
        _, submission = next(self.jira.discover(self.im, entry))
        self.assertEqual(submission.data["comment"], "- foobar\n- spam\n- eggs")

    def test_30_30(self):
        entries = EntriesGen(start=self.now, duration=timedelta(seconds=90)).take(100)
        _random = (x / 100 for x in range(100)).__next__

        with unittest.mock.patch("random.random", _random):
            durations = [
                submission.data["timeSpentSeconds"]
                for _, submission in self.jira.discover(self.im, *entries)
            ]

        avg = sum(durations) / len(durations)
        self.assertAlmostEqual(avg, 90.0)

    def test_10_50(self):
        entries = EntriesGen(start=self.now, duration=timedelta(seconds=72)).take(100)
        _random = (x / 100 for x in range(100)).__next__

        with unittest.mock.patch("random.random", _random):
            durations = [
                submission.data["timeSpentSeconds"]
                for _, submission in self.jira.discover(self.im, *entries)
            ]

        avg = sum(durations) / len(durations)
        self.assertAlmostEqual(avg, 72.0)


@pytest.mark.asyncio
async def test_jira_posting_error(event_loop, tmp_path):
    from impetuous.config import ConfigParser
    from impetuous.ext.jira import JiraOverUnix
    import asyncio
    from aiohttp import web

    async def jira_ok(request):
        await asyncio.sleep(0.05)
        return web.json_response({"great work": "keep it up"})

    async def jira_500(request):
        await asyncio.sleep(0.05)
        raise Exception("nahhh")

    routes = [
        web.post("/rest/api/2/issue/TEST-0/worklog", jira_ok),
        web.post("/rest/api/2/issue/TEST-1/worklog", jira_500),
        web.post("/rest/api/2/issue/TEST-2/worklog", jira_ok),
    ]

    _, jira_runner, jira_app = await unix_http_listener(tmp_path / "jira.sock", routes)

    im = Impetuous()
    jira = JiraOverUnix.from_config(
        {
            "api": "jira",
            "server": "http://localhost",
            "basic_auth": "admin:hunter2",
            "pattern": r"(?:TEST-\d+)",
            "socket": str(tmp_path / "jira.sock"),
        }
    )

    # poor man's im_post in lieu of refactoring that so it's testable...

    agent = await jira.agent(im)
    taskythings = {
        submission.key: event_loop.create_task(agent.submit(submission))
        for entry, submission in jira.discover(im, *EntriesGen().take(4))
    }
    done, pending = await asyncio.wait(taskythings.values())
    assert not pending
    await agent.close()
    await jira_runner.cleanup()

    assert taskythings["TEST-0"].result() == {"great work": "keep it up"}
    with pytest.raises(LudicrousConditions, match="Server Error"):
        assert taskythings["TEST-1"].result()
    assert taskythings["TEST-2"].result() == {"great work": "keep it up"}
    with pytest.raises(LudicrousConditions, match="Not Found"):
        assert taskythings["TEST-3"].result()


class EntriesGen(object):
    default_start = datetime.datetime(2010, 5, 4, 1, 0, 0, tzinfo=pytz.utc)

    def __init__(
        self, *, start=default_start, duration=timedelta(minutes=1), interval=None
    ):
        if duration is None:
            duration = timedelta(seconds=0)
        if interval is None:
            interval = timedelta(seconds=0)
        self.start = start
        self.duration = duration
        self.interval = interval
        self.count = itertools.count(0)

    def __iter__(self):
        return self

    def __next__(self):
        i = next(self.count)
        entry_start = self.start + (self.duration + self.interval) * i
        return Entry(
            text="TEST-%i" % i,
            start=entry_start,
            end=entry_start + self.duration,
            comment=None,
        )

    def take(self, n):
        return itertools.islice(self, n)


async def unix_http_listener(sock_path, routes):
    from aiohttp import web

    app = web.Application()
    app.add_routes(routes)

    runner = web.AppRunner(app)
    await runner.setup()

    site = web.UnixSite(runner, sock_path)
    await site.start()
    return site, runner, app


def test_decode_nothing():
    cfg = configparser.ConfigParser()
    cfg["foo"] = {}
    cfg["foo"]["bar"] = "spam"
    coded = CodedValue.from_config(cfg["foo"], "bar")
    assert coded.encoded == "spam"
    assert coded.decoded == "spam"


def test_decode_base64():
    cfg = configparser.ConfigParser()
    cfg["foo"] = {}
    cfg["foo"]["bar.base64"] = "YzNCaGJRPT0K\n"
    coded = CodedValue.from_config(cfg["foo"], "bar")
    assert CodedValue.plain("spam").encode("base64") == coded
    assert coded.encoded == "YzNCaGJRPT0K\n"
    assert coded.decoded == "spam"
    assert "spam" == CodedValue.decode_from_config(cfg["foo"], "bar")


def test_encode_zlib():
    assert CodedValue.plain("foo").encode("zlib").encoded == "eJxLy88HAAKCAUU=\n"
