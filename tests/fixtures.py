import logging
from datetime import datetime, timedelta, timezone
from itertools import count
from pprint import pprint

import attr
import pytest
from sqlalchemy.sql import expression

from impetuous.data import entry_t, metadata

logger = logging.getLogger(__name__)


@pytest.fixture
def engine():
    from sqlalchemy import create_engine
    return create_engine('sqlite:///:memory:', echo=True)


@pytest.fixture
def conn(engine):
    return engine.connect()


@pytest.fixture
def schema(conn):
    metadata.create_all(conn)


def textgen():
    counter = count()
    return ("text %s" % c for c in counter)


@attr.s
class DataGen(object):
    conn = attr.ib()
    text = attr.ib(default=attr.Factory(textgen))

    def sel_entry(self, id, *fields):
        q = expression.select(entry_t.c[f] for f in fields)
        return self.conn.execute(q).first()

    def ins_entry(self, **values):
        if 'text' not in values:
            values['text'] = next(self.text)
        exp = expression.insert(entry_t).values(values)
        return self.conn.execute(exp).inserted_primary_key

    def upd_entry(self, id, **values):
        exp = expression.update(
            entry_t,
            whereclause=entry_t.c.id==id
        ).values(values)
        assert self.conn.execute(exp)


@pytest.fixture
def gen(schema, conn):
    return DataGen(conn)


@pytest.fixture
def agent():
    class agent:
        is_authenticated = lambda: "sure!"
    return agent
